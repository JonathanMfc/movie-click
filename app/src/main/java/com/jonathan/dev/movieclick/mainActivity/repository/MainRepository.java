package com.jonathan.dev.movieclick.mainActivity.repository;

/**
 * Created by mfc on 31/10/17.
 */

public interface MainRepository {

    void getDataResultSearchMovie(String movie);
    void getDataResultSearchTv(String tvShow);
}
