package com.jonathan.dev.movieclick.mainActivity.presenter;

import com.jonathan.dev.movieclick.mainActivity.interactor.TvShowInteractor;
import com.jonathan.dev.movieclick.mainActivity.interactor.TvShowInteractorImpl;
import com.jonathan.dev.movieclick.model.TvShow;
import com.jonathan.dev.movieclick.mainActivity.view.fragment.TvShowView;

import java.util.ArrayList;

/**
 * Created by mfc on 30/10/17.
 */

public class TvShowPresenterImpl implements TvShowPresenter {

    private TvShowView tvShowView;
    private TvShowInteractor tvShowInteractor;

    public TvShowPresenterImpl(TvShowView tvShowView) {
        this.tvShowView = tvShowView;
        tvShowInteractor = new TvShowInteractorImpl(this);
    }


    @Override
    public void getDataTvPopular() {
        tvShowView.showProgress();
        tvShowInteractor.getDataTvPopular();
    }

    @Override
    public void getDataTvTopRated() {
        tvShowView.showProgress();
        tvShowInteractor.getDataTvTopRated();

    }

    @Override
    public void showDataTvPopular(ArrayList<TvShow> tvShowsPopular) {
        tvShowView.hideProgress();
        tvShowView.showDataTvPopular(tvShowsPopular);
    }

    @Override
    public void showDataTvTopRated(ArrayList<TvShow> tvShowsTopRated) {
        tvShowView.hideProgress();
        tvShowView.showDataTvTopRated(tvShowsTopRated);
    }

    @Override
    public void tvError(String error) {
        tvShowView.hideProgress();
        tvShowView.tvError(error);
    }
}
