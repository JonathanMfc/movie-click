package com.jonathan.dev.movieclick.mainActivity.interactor;

import com.jonathan.dev.movieclick.mainActivity.presenter.MainPresenter;
import com.jonathan.dev.movieclick.mainActivity.repository.MainRepository;
import com.jonathan.dev.movieclick.mainActivity.repository.MainRepositoryImpl;

/**
 * Created by mfc on 31/10/17.
 */

public class MainInteractorImpl implements MainInteractor {

    private MainPresenter mainPresenter;
    private MainRepository mainRepository;

    public MainInteractorImpl(MainPresenter mainPresenter) {
        this.mainPresenter = mainPresenter;
        mainRepository = new MainRepositoryImpl(mainPresenter);
    }

    @Override
    public void getDataResultSearchMovie(String movie) {
        mainRepository.getDataResultSearchMovie(movie);

    }

    @Override
    public void getDataResultSearchTv(String tvShow) {
        mainRepository.getDataResultSearchTv(tvShow);
    }
}
