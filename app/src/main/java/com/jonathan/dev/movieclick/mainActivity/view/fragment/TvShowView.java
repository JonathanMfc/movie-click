package com.jonathan.dev.movieclick.mainActivity.view.fragment;

import com.jonathan.dev.movieclick.model.TvShow;

import java.util.ArrayList;

/**
 * Created by mfc on 30/10/17.
 */

public interface TvShowView {

    void getDataTvPopular();
    void getDataTvTopRated();
    void showDataTvPopular(ArrayList<TvShow> tvShowsPopular);
    void showDataTvTopRated(ArrayList<TvShow> tvShowsTopRated);
    void tvError(String error);
    void showProgress();
    void hideProgress();
}
