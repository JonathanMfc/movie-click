package com.jonathan.dev.movieclick.mainActivity.interactor;

/**
 * Created by mfc on 31/10/17.
 */

public interface MainInteractor {

    void getDataResultSearchMovie(String movie);
    void getDataResultSearchTv(String tvShow);
}
