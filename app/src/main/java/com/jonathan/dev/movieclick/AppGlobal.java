package com.jonathan.dev.movieclick;

import android.app.Application;

import com.jonathan.dev.movieclick.broadcast.ConnectivityReceiver;

/**
 * Created by mfc on 1/11/17.
 */

public class AppGlobal extends Application {

    private static AppGlobal mInstance;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized AppGlobal getInstance(){
        return mInstance;
    }
    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }



}
