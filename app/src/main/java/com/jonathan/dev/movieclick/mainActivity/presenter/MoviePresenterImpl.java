package com.jonathan.dev.movieclick.mainActivity.presenter;

import com.jonathan.dev.movieclick.mainActivity.interactor.MovieInteractor;
import com.jonathan.dev.movieclick.mainActivity.interactor.MovieInteractorImpl;
import com.jonathan.dev.movieclick.mainActivity.view.fragment.MovieView;
import com.jonathan.dev.movieclick.model.Movie;

import java.util.ArrayList;

/**
 * Created by mfc on 30/10/17.
 */

public class MoviePresenterImpl implements MoviePresenter {

    private MovieView movieView;
    private MovieInteractor movieInteractor;

    public MoviePresenterImpl(MovieView movieView) {
        this.movieView = movieView;
        movieInteractor = new MovieInteractorImpl(this);
    }



    @Override
    public void getDataMoviePopular() {
        movieView.showProgress();
        movieInteractor.getDataMoviePopular();
    }

    @Override
    public void getDataMovieTopRated() {
        movieView.showProgress();
        movieInteractor.getDataMovieTopRated();
    }

    @Override
    public void getDataMovieUpcoming() {
        movieView.showProgress();
        movieInteractor.getDataMovieUpcoming();
    }


    @Override
    public void showDataMoviePopular(ArrayList<Movie> moviesPopular) {
        movieView.hideProgress();
        movieView.showDataMoviePopular(moviesPopular);
    }

    @Override
    public void showDataMovieTopRated(ArrayList<Movie> moviesTopRated) {
        movieView.hideProgress();
        movieView.showDataMovieTopRated(moviesTopRated);
    }

    @Override
    public void showDataMovieUpcoming(ArrayList<Movie> moviesUpcoming) {
        movieView.hideProgress();
        movieView.showDataMovieUpcoming(moviesUpcoming);
    }



    @Override
    public void movieError(String error) {
        movieView.hideProgress();
        movieView.movieError(error);
    }
}
