package com.jonathan.dev.movieclick.mainActivity.repository;

import com.jonathan.dev.movieclick.BuildConfig;
import com.jonathan.dev.movieclick.mainActivity.presenter.MainPresenter;
import com.jonathan.dev.movieclick.model.MovieResponse;
import com.jonathan.dev.movieclick.model.TvShowResponse;
import com.jonathan.dev.movieclick.theMovieDatabaseAPI.Constants;
import com.jonathan.dev.movieclick.theMovieDatabaseAPI.RestApiAdapter;
import com.jonathan.dev.movieclick.theMovieDatabaseAPI.Service;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mfc on 31/10/17.
 */

public class MainRepositoryImpl implements MainRepository {

    private MainPresenter mainPresenter;
    Service service = RestApiAdapter.createService(Service.class);

    public MainRepositoryImpl(MainPresenter mainPresenter) {
        this.mainPresenter = mainPresenter;
    }


    @Override
    public void getDataResultSearchMovie(String movie) {
        Call<MovieResponse> callMovieSearch = service.getMovieSearch(movie, BuildConfig.THE_MOVIE_DATABASE_API, Constants.LANGUAGES,Constants.SEARCH_MOVIE_PAGE);
        callMovieSearch.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if (response.isSuccessful() && response.body().getStatus_message()==null){
                    mainPresenter.showDataResultSearchMovie(response.body().getResults());
                    Constants.SEARCH_MOVIE_PAGE++;
                }else {
                    mainPresenter.resultError(response.body().getStatus_message());
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                mainPresenter.resultError(t.getMessage());
            }
        });
    }

    @Override
    public void getDataResultSearchTv(String tvShow) {
        Call<TvShowResponse> callTvSearch = service.getTvShowSearch(tvShow,BuildConfig.THE_MOVIE_DATABASE_API,Constants.LANGUAGES,Constants.SEARCH_TV_PAGE);
        callTvSearch.enqueue(new Callback<TvShowResponse>() {
            @Override
            public void onResponse(Call<TvShowResponse> call, Response<TvShowResponse> response) {
                if (response.isSuccessful() && response.body().getStatus_message()==null){
                    mainPresenter.showDataResultSearchTv(response.body().getResults());
                    Constants.SEARCH_TV_PAGE++;
                }else {
                    mainPresenter.resultError(response.body().getStatus_message());
                }
            }

            @Override
            public void onFailure(Call<TvShowResponse> call, Throwable t) {
                mainPresenter.resultError(t.getMessage());
            }
        });
    }
}
