package com.jonathan.dev.movieclick.detailActivity.repository;

/**
 * Created by mfc on 31/10/17.
 */

public interface DetailRepository {

    void getDataMovieRecommendations(int movie_id);
    void getDataTvRecommendations(int tv_id);
}
