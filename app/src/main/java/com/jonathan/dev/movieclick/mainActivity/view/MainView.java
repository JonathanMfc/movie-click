package com.jonathan.dev.movieclick.mainActivity.view;

import com.jonathan.dev.movieclick.model.Movie;
import com.jonathan.dev.movieclick.model.TvShow;

import java.util.ArrayList;

/**
 * Created by mfc on 31/10/17.
 */

public interface MainView {

    void getDataResultSearchMovie(String movie);
    void getDataResultSearchTv(String tvShow);
    void showDataResultSearchMovie(ArrayList<Movie> movies);
    void showDataResultSearchTv(ArrayList<TvShow> tvShows);
    void resultError(String error);
    void showProgress();
    void hideProgress();
}
