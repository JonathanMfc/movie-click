package com.jonathan.dev.movieclick.detailActivity.interactor;

/**
 * Created by mfc on 31/10/17.
 */

public interface DetailInteractor {

    void getDataMovieRecommendations(int movie_id);
    void getDataTvRecommendations(int tv_id);
}
