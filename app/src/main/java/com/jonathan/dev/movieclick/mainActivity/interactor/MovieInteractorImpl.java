package com.jonathan.dev.movieclick.mainActivity.interactor;

import com.jonathan.dev.movieclick.mainActivity.presenter.MoviePresenter;
import com.jonathan.dev.movieclick.mainActivity.repository.MovieRepository;
import com.jonathan.dev.movieclick.mainActivity.repository.MovieRepositoryImpl;

/**
 * Created by mfc on 30/10/17.
 */

public class MovieInteractorImpl implements MovieInteractor {

    private MoviePresenter moviePresenter;
    private MovieRepository movieRepository;

    public MovieInteractorImpl(MoviePresenter moviePresenter) {
        this.moviePresenter = moviePresenter;
        movieRepository = new MovieRepositoryImpl(moviePresenter);
    }


    @Override
    public void getDataMoviePopular() {
        movieRepository.getDataMoviePopular();
    }

    @Override
    public void getDataMovieTopRated() {
        movieRepository.getDataMovieTopRated();
    }

    @Override
    public void getDataMovieUpcoming() {
        movieRepository.getDataMovieUpcoming();
    }
}
