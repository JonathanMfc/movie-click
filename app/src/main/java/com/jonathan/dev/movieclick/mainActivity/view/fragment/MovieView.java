package com.jonathan.dev.movieclick.mainActivity.view.fragment;

import com.jonathan.dev.movieclick.model.Movie;

import java.util.ArrayList;

/**
 * Created by mfc on 30/10/17.
 */

public interface MovieView {

    void getDataMoviePopular();
    void getDataMovieTopRated();
    void getDataMovieUpcoming();
    void showDataMoviePopular(ArrayList<Movie> moviesPopular);
    void showDataMovieTopRated(ArrayList<Movie> moviesTopRated);
    void showDataMovieUpcoming(ArrayList<Movie> moviesUpcoming);
    void movieError(String error);
    void showProgress();
    void hideProgress();
}
