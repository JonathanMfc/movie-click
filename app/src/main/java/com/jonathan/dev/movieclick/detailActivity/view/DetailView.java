package com.jonathan.dev.movieclick.detailActivity.view;

import com.jonathan.dev.movieclick.model.Movie;
import com.jonathan.dev.movieclick.model.TvShow;

import java.util.ArrayList;

/**
 * Created by mfc on 31/10/17.
 */

public interface DetailView {

    void getDataMovieRecommendations(int movie_id);
    void getDataTvRecommendations(int tv_id);
    void showDataMovieRecommendations(ArrayList<Movie> movies);
    void showDataTvRecommendations(ArrayList<TvShow> tvShows);
    void recommendationsError(String error);
    void showProgress();
    void hideProgress();
}
