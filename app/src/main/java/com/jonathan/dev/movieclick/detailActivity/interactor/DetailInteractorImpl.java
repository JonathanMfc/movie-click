package com.jonathan.dev.movieclick.detailActivity.interactor;

import com.jonathan.dev.movieclick.detailActivity.presenter.DetailPresenter;
import com.jonathan.dev.movieclick.detailActivity.repository.DetailRepository;
import com.jonathan.dev.movieclick.detailActivity.repository.DetailRepositoryImpl;

/**
 * Created by mfc on 31/10/17.
 */

public class DetailInteractorImpl implements DetailInteractor{

    private DetailPresenter detailPresenter;
    private DetailRepository detailRepository;

    public DetailInteractorImpl(DetailPresenter detailPresenter) {
        this.detailPresenter = detailPresenter;
        detailRepository = new DetailRepositoryImpl(detailPresenter);
    }

    @Override
    public void getDataMovieRecommendations(int movie_id) {
        detailRepository.getDataMovieRecommendations(movie_id);
    }

    @Override
    public void getDataTvRecommendations(int tv_id) {
        detailRepository.getDataTvRecommendations(tv_id);
    }
}
