package com.jonathan.dev.movieclick.mainActivity.interactor;

/**
 * Created by mfc on 30/10/17.
 */

public interface MovieInteractor {


    void getDataMoviePopular();
    void getDataMovieTopRated();
    void getDataMovieUpcoming();
}
