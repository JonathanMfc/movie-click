package com.jonathan.dev.movieclick.detailActivity.presenter;

import com.jonathan.dev.movieclick.detailActivity.interactor.DetailInteractor;
import com.jonathan.dev.movieclick.detailActivity.interactor.DetailInteractorImpl;
import com.jonathan.dev.movieclick.detailActivity.view.DetailView;
import com.jonathan.dev.movieclick.model.Movie;
import com.jonathan.dev.movieclick.model.TvShow;

import java.util.ArrayList;

/**
 * Created by mfc on 31/10/17.
 */

public class DetailPresenterImpl implements DetailPresenter{

    private DetailView detailView;
    private DetailInteractor detailInteractor;

    public DetailPresenterImpl(DetailView detailView) {
        this.detailView = detailView;
        detailInteractor = new DetailInteractorImpl(this);
    }

    @Override
    public void getDataMovieRecommendations(int movie_id) {
        detailView.showProgress();
        detailInteractor.getDataMovieRecommendations(movie_id);
    }

    @Override
    public void getDataTvRecommendations(int tv_id) {
        detailView.showProgress();
        detailInteractor.getDataTvRecommendations(tv_id);

    }

    @Override
    public void showDataMovieRecommendations(ArrayList<Movie> movies) {
        detailView.hideProgress();
        detailView.showDataMovieRecommendations(movies);
    }

    @Override
    public void showDataTvRecommendations(ArrayList<TvShow> tvShows) {
        detailView.hideProgress();
        detailView.showDataTvRecommendations(tvShows);
    }

    @Override
    public void recommendationsError(String error) {
        detailView.hideProgress();
        detailView.recommendationsError(error);
    }
}
