package com.jonathan.dev.movieclick.mainActivity.repository;

/**
 * Created by mfc on 30/10/17.
 */

public interface MovieRepository {


    void getDataMoviePopular();
    void getDataMovieTopRated();
    void getDataMovieUpcoming();
}
