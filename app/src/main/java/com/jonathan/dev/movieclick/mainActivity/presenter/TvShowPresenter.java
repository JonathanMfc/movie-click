package com.jonathan.dev.movieclick.mainActivity.presenter;

import com.jonathan.dev.movieclick.model.TvShow;

import java.util.ArrayList;

/**
 * Created by mfc on 30/10/17.
 */

public interface TvShowPresenter {

    void getDataTvPopular();
    void getDataTvTopRated();
    void showDataTvPopular(ArrayList<TvShow> tvShowsPopular);
    void showDataTvTopRated(ArrayList<TvShow> tvShowsTopRated);
    void tvError(String error);
}
