package com.jonathan.dev.movieclick.mainActivity.interactor;

import com.jonathan.dev.movieclick.mainActivity.presenter.TvShowPresenter;
import com.jonathan.dev.movieclick.mainActivity.repository.TvShowRepository;
import com.jonathan.dev.movieclick.mainActivity.repository.TvShowRepositoryImpl;

/**
 * Created by mfc on 30/10/17.
 */

public class TvShowInteractorImpl implements TvShowInteractor {

    private TvShowPresenter tvShowPresenter;
    private TvShowRepository tvShowRepository;

    public TvShowInteractorImpl(TvShowPresenter tvShowPresenter) {
        this.tvShowPresenter = tvShowPresenter;
        tvShowRepository = new TvShowRepositoryImpl(tvShowPresenter);
    }


    @Override
    public void getDataTvPopular() {
        tvShowRepository.getDataTvPopular();

    }

    @Override
    public void getDataTvTopRated() {
        tvShowRepository.getDataTvTopRated();
    }
}
