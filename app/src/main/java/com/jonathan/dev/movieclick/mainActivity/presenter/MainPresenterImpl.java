package com.jonathan.dev.movieclick.mainActivity.presenter;

import com.jonathan.dev.movieclick.mainActivity.interactor.MainInteractor;
import com.jonathan.dev.movieclick.mainActivity.interactor.MainInteractorImpl;
import com.jonathan.dev.movieclick.mainActivity.view.MainView;
import com.jonathan.dev.movieclick.model.Movie;
import com.jonathan.dev.movieclick.model.TvShow;

import java.util.ArrayList;

/**
 * Created by mfc on 31/10/17.
 */

public class MainPresenterImpl implements MainPresenter {

    private MainView mainView;
    private MainInteractor mainInteractor;

    public MainPresenterImpl(MainView mainView) {
        this.mainView = mainView;
        mainInteractor = new MainInteractorImpl(this);
    }

    @Override
    public void getDataResultSearchMovie(String movie) {
        mainView.showProgress();
        mainInteractor.getDataResultSearchMovie(movie);

    }

    @Override
    public void getDataResultSearchTv(String tvShow) {
        mainView.showProgress();
        mainInteractor.getDataResultSearchTv(tvShow);

    }

    @Override
    public void showDataResultSearchMovie(ArrayList<Movie> movies) {
        mainView.hideProgress();
        mainView.showDataResultSearchMovie(movies);
    }

    @Override
    public void showDataResultSearchTv(ArrayList<TvShow> tvShows) {

        mainView.hideProgress();
        mainView.showDataResultSearchTv(tvShows);
    }

    @Override
    public void resultError(String error) {
        mainView.hideProgress();
        mainView.resultError(error);
    }
}
